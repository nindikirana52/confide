package com.confide.confide.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.confide.confide.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}