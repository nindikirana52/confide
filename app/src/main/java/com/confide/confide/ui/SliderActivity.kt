package com.confide.confide.ui

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import com.confide.confide.R
import com.confide.confide.ui.adapter.SliderPetunjukAdapter
import com.smarteist.autoimageslider.IndicatorAnimations
import com.smarteist.autoimageslider.SliderAnimations
import kotlinx.android.synthetic.main.activity_slider.*

class SliderActivity : AppCompatActivity() {

    val listImage = arrayOf(R.drawable.university, R.drawable.comment, R.drawable.poin)
    val listPetunjuk = listOf(
            "Rule of Login",
            "Rule of Post and Comment",
            "Rule of Report")
    val listDeskripsiJudul = listOf(
            "Login menggunakan nim dan password yang terdaftar pada Universitas.",
            "Dilarang menggunakan kata yang mengandung unsur pornografi, sara, rasisme, jual-beli, dan spam.",
            "Bagi yang melanggar aturan aplikasi maka akan mendapat tambahan poin pelanggaran.")

    lateinit var sliderPetunjukAdapter: SliderPetunjukAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (SharedPrefUtil.getBoolean("dark_mode")) {
            setTheme(R.style.NoActionBarDark)
        }
        setContentView(R.layout.activity_slider)
        if (SharedPrefUtil.getBoolean("dark_mode")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.hitam)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window!!.decorView.systemUiVisibility = 0
            }
        }
        else{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = ContextCompat.getColor(this, R.color.putih)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                window!!.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
        }
        sliderPetunjukAdapter = SliderPetunjukAdapter(this, listImage, listPetunjuk, listDeskripsiJudul)
        sliderTutorial.setIndicatorAnimation(IndicatorAnimations.WORM)
        sli.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        imgSliderPetunjuk.sliderAdapter = sliderPetunjukAdapter
    }
}