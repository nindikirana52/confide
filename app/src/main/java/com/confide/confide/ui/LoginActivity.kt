package com.confide.confide.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.confide.confide.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initView()
    }

    private fun initView() {
        btGotoRegister.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v) {
            btGotoRegister -> {
                startActivity(Intent(this, RegisterActivity::class.java))
            }
        }
    }
}